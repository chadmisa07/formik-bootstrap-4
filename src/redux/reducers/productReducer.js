import * as constants from '../../common/constants';

export default function reducer(state = [], {type, payload}) {
  const curState = [...state];
  switch (type) {
    case constants.ADD_PRODUCT:
      curState.push(payload);
      return [...curState];

    case constants.EDIT_PRODUCT:
      const index = payload.index;
      const obj = {
        id: payload.id,
        name: payload.name,
        description: payload.description,
        imageUrl: payload.imageUrl
      };
      curState[index] = obj;
      return [...curState];

    case constants.DELETE_PRODUCT:
      curState.splice(payload, 1);
      return [...curState];

    default:
      return state;
  }
}
