import UPDATE_USER from '../../common/constants';

export default function reducer(state = {}, {type, payload}) {
  switch (type) {
    case UPDATE_USER:
      return payload;

    default:
      return state;
  }
}
