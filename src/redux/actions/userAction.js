import {UPDATE_USER} from '../../common/constants';

export default function updateUser(newUser) {
  return {
    type: UPDATE_USER,
    payload: newUser
  };
}
