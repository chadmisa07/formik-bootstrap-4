import * as constants from '../../common/constants';

export function deleteProduct(index) {
  return {
    type: constants.DELETE_PRODUCT,
    payload: index
  };
}

export function addProduct(product) {
  return {
    type: constants.ADD_PRODUCT,
    payload: product
  };
}

export function editProduct(product) {
  return {
    type: constants.EDIT_PRODUCT,
    payload: product
  };
}
