import {createStore, combineReducers} from 'redux';
import userReducer from './reducers/userReducer';
import productReducer from './reducers/productReducer';

const allReducers = combineReducers({
  user: userReducer,
  products: productReducer
});

const store = createStore(allReducers, {
  products: [
    {
      id: 1,
      name: 'Headset',
      description: 'The quick brown fox jump over the lazy dog',
      imageUrl:
        'https://www.jbl.com/on/demandware.static/-/Sites-masterCatalog_Harman/default/dwd6661a5e/450BT_black_angle_01-1606x1606px.png'
    },
    {
      id: 2,
      name: 'Mouse pad',
      description: 'Very nice',
      imageUrl:
        'https://cf5.s3.souqcdn.com/item/2016/10/13/11/68/12/12/item_XL_11681212_16855806.jpg'
    }
  ],
  user: {name: 'Chad', age: 25}
});

export default store;
