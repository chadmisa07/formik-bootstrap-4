import React, {Component} from 'react';
import {Modal, Button, Row, Col} from 'react-bootstrap';
import {Form, Field, Formik} from 'formik';
import Input from './components/Input';
import TextArea from './components/TextArea';
import _ from 'lodash';

function initialValues(product) {
  if (!_.isEmpty(product)) {
    return {
      productName: product.name,
      productImage: product.imageUrl,
      productDescription: product.description
    };
  }

  return {
    productImage: '',
    productName: '',
    productDescription: ''
  };
}

export default class ProductModal extends Component {
  render() {
    const {product, doFormSubmit, validate, show, handleClose} = this.props;
    return (
      <Modal show={show} onHide={handleClose}>
        <Formik
          initialValues={initialValues(product)}
          validate={validate}
          onSubmit={(values, {resetForm}) => {
            doFormSubmit(values);
            resetForm({});
          }}
        >
          {({dirty, isValidating, isSubmitting, handleSubmit}) => (
            <Form onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>Add Product</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Row>
                  <Col md={12}>
                    <Field
                      name="productImage"
                      component={Input}
                      label="Product Image"
                      help="Product Image"
                      placeholder="https://www.google.com/dota.jpg"
                    />
                    <Field
                      name="productName"
                      component={Input}
                      label="Product Name"
                      help="Product Name"
                      placeholder="Enter Product Name"
                    />
                    <Field
                      component={TextArea}
                      name="productDescription"
                      label="Product Description"
                      placeholder="Enter Product Description"
                      help="Product Description"
                    />
                  </Col>
                </Row>
              </Modal.Body>
              <Modal.Footer>
                <Button type="submit" variant="primary" disabled={!dirty || isSubmitting}>
                  Save
                </Button>
              </Modal.Footer>
            </Form>
          )}
        </Formik>
      </Modal>
    );
  }
}
