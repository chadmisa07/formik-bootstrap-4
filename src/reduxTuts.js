import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Container, Card, Row, Button, Col, ProgressBar} from 'react-bootstrap';
import _ from 'lodash';
import ProductModal from './ProductModal';
import * as ProductAction from './redux/actions/productAction';

const mapStateToProps = state => ({
  products: state.products,
  user: state.user
});

// function initialValues(product) {
//   if (product) {
//     return {
//       productName: product.name,
//       productImage: product.imageUrl,
//       productDescription: product.description
//     };
//   }

//   return {
//     productImage: '',
//     productName: '',
//     productDescription: ''
//   };
// }

// const initialValues = {
//   productImage: '',
//   productName: '',
//   productDescription: ''
// };

class ReduxTuts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      product: {}
    };
  }

  handleClose = () => {
    this.setState({show: false, product: {}});
  };

  handleShow = () => {
    this.setState({show: true});
  };

  setProduct = (product, index) => {
    this.setState({product, index, show: true});
  };

  resetProduct = () => {
    this.setState({product: {}, show: false});
  };

  render() {
    // console.log(this.props);
    const {user, products} = this.props;
    const now = 60;
    const cards = _.map(products, (product, index) => {
      return (
        <Col md={3} key={index} style={{marginTop: '10px'}}>
          <Card className="mx-2 mb-3">
            <Card.Img variant="top" src={product.imageUrl} />
            <div id="b" />
            <div id="c">
              <ProgressBar striped variant="warning" now={now} label={`${now}%`} />
            </div>
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>{product.description}</Card.Text>
              <Button variant="primary" onClick={this.setProduct.bind(this, product, index)}>
                Edit
              </Button>{' '}
              <Button variant="danger" data-index={index} onClick={this.doDelete} className="mx-2">
                Delete
              </Button>
            </Card.Body>
          </Card>
        </Col>
      );
    });
    return (
      <Container>
        <Row>
          <Col md={12}>
            Welcome <b>{user.name}!</b>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <h2>Available Products</h2>
          </Col>
        </Row>

        <Row>{cards}</Row>
        <Row />
        <br />
        <br />
        <Row>
          <Col md={12}>
            <Button variant="primary" onClick={this.handleShow} className="mx-2">
              Add New Product
            </Button>

            <ProductModal
              product={this.state.product}
              doFormSubmit={this.doFormSubmit}
              validate={this.validate}
              handleClose={this.handleClose}
              show={this.state.show}
            />

            {/* <Modal show={this.state.show} onHide={this.handleClose}>
              <Formik
                initialValues={initialValues}
                validate={this.validate}
                onSubmit={(values, {resetForm}) => {
                  this.doFormSubmit(values);
                  resetForm({});
                }}
              >
                {({errors, touched, dirty, isValid, isSubmitting, handleSubmit}) => (
                  <Form onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                      <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Row>
                        <Col md={12}>
                          <Field
                            name="productImage"
                            component={Input}
                            label="Product Image"
                            help="Product Image"
                            placeholder="https://www.google.com/dota.jpg"
                          />
                          <Field
                            name="productName"
                            component={Input}
                            label="Product Name"
                            help="Product Name"
                            placeholder="Enter Product Name"
                          />
                          <Field
                            component={TextArea}
                            name="productDescription"
                            label="Product Description"
                            placeholder="Enter Product Description"
                            help="Product Description"
                          />
                        </Col>
                      </Row>
                    </Modal.Body>
                    <Modal.Footer><Modal show={this.state.show} onHide={this.handleClose}>
              <Formik
                initialValues={initialValues}
                validate={this.validate}
                onSubmit={(values, {resetForm}) => {
                  this.doFormSubmit(values);
                  resetForm({});
                }}
              >
                {({errors, touched, dirty, isValid, isSubmitting, handleSubmit}) => (
                  <Form onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                      <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Row>
                        <Col md={12}>
                          <Field
                            name="productImage"
                            component={Input}
                            label="Product Image"
                            help="Product Image"
                            placeholder="https://www.google.com/dota.jpg"
                          />
                          <Field
                            name="productName"
                            component={Input}
                            label="Product Name"
                            help="Product Name"
                            placeholder="Enter Product Name"
                          />
                          <Field
                            component={TextArea}
                            name="productDescription"
                            label="Product Description"
                            placeholder="Enter Product Description"
                            help="Product Description"
                          />
                        </Col>
                      </Row>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button type="submit" variant="primary" disabled={!dirty || isSubmitting}>
                        Save
                      </Button>
                    </Modal.Footer>
                  </Form>
                )}
              </Formik>
            </Modal>
                      <Button type="submit" variant="primary" disabled={!dirty || isSubmitting}>
                        Save
                      </Button>
                    </Modal.Footer>
                  </Form>
                )}
              </Formik>
            </Modal> */}
          </Col>
        </Row>
      </Container>
    );
  }

  doDelete = e => {
    this.props.dispatch(ProductAction.deleteProduct(e.target.dataset.index));
  };

  doFormSubmit = values => {
    const {products} = this.props;
    const id = products.length + 1;

    //add product if the product state is empty
    if (!_.isEmpty(this.state.product)) {
      const obj = {
        index: this.state.index,
        id: id,
        imageUrl: values.productImage,
        name: values.productName,
        description: values.productDescription
      };
      // this.props.dispatch({type: constants.EDIT_PRODUCT, payload: obj});
      this.props.dispatch(ProductAction.editProduct(obj));
    } else {
      const obj = {
        id: id,
        imageUrl: values.productImage,
        name: values.productName,
        description: values.productDescription
      };
      // this.props.dispatch({type: constants.ADD_PRODUCT, payload: obj});
      this.props.dispatch(ProductAction.addProduct(obj));
    }

    this.handleClose();
  };

  validate = values => {
    let errors = {};
    if (!values.productImage.startsWith('https://')) {
      errors.productImage = 'Enter a valid image url';
    }

    if (values.productName === '') {
      errors.productName = 'Required';
    }

    if (values.productDescription === '') {
      errors.productDescription = 'Required';
    }

    return errors;
  };
}

export default connect(mapStateToProps)(ReduxTuts);
