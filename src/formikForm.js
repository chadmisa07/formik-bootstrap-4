import React, {Component} from 'react';
import {Container} from 'react-bootstrap';
import {Formik, Form, Field} from 'formik';
import Input from './components/Input';
import Select from './components/Select';
import TextArea from './components/TextArea';
import FormSubmit from './components/FormSubmit';
// import RichTextEditor from './components/RichTextEditor';
import RichTextEditorQuill from './components/RichTextEditorQuill';
import DateTimePicker from './components/DateTimePicker';
import ImagePicker from './components/ImagePicker';
import UserLink from './components/UserLink';
import TimezoneSelect from './components/TimezoneSelect';
import MatcherinoUserPicker from './components/MatcherinoUserPicker';
import ReCaptcha from './components/ReCaptcha';

const initialValues = {
  firstName: '',
  lastName: '',
  age: '0',
  description: '',
  description2: '',
  startAt: null,
  heroImage: '',
  timezone: '',
  favoriteFood: '',
  recaptcha: false
};

const favoriteFood = [
  {label: 'Fried Chicken', value: 'manok'},
  {label: 'Porkchop', value: 'porkchop'},
  {label: 'Fried Bangus', value: 'fried bangus'}
];

const user = {
  id: 100,
  userName: 'chadmisa07',
  displayName: 'Chad The Great'
};

const users = [
  {
    id: 100,
    userName: 'chadmisa07',
    displayName: 'Chad The Great',
    label: 'chadmisa07',
    value: 'Chad'
  },
  {
    id: 200,
    userName: 'kentferolino',
    displayName: 'Kent The Great',
    label: 'kentferolino',
    value: 'Kent'
  },
  {
    id: 300,
    userName: 'mgutz',
    displayName: 'Mario The Great',
    label: 'mgutz',
    value: 'Mario'
  }
];
class FormikForm extends Component {
  doFormSubmit(form) {
    console.log('FORM VALUES >>>', form);
  }

  render() {
    return (
      <Container>
        <Formik
          initialValues={initialValues}
          validate={this.validate}
          onSubmit={(values, {setSubmitting, setErrors, setStatus, resetForm}) => {
            this.doFormSubmit(values);
            resetForm({});
          }}
        >
          {({errors, touched, dirty, isValidating, isSubmitting, handleSubmit}) => (
            <Form onSubmit={handleSubmit}>
              <FormSubmit status={!dirty || isSubmitting}>
                <Field
                  component={Input}
                  label="First Name"
                  // help="First Name"
                  type="text"
                  name="firstName"
                />

                <Field
                  component={Input}
                  placeholder="Enter Last Name"
                  label="Last Name"
                  help="Last Name"
                  name="lastName"
                />

                <Field
                  component={Input}
                  placeholder="Enter your age"
                  label="Age"
                  type="number"
                  name="age"
                />

                <Field
                  component={Select}
                  label="Favorite Food"
                  name="favoriteFood"
                  option={favoriteFood}
                />

                <Field
                  component={TextArea}
                  placeholder="Enter a description"
                  label="Description"
                  name="description"
                />

                {/* <Field
                component={RichTextEditor}
                placeholder="Enter a description"
                label="Description2"
                name="description2"
              /> */}

                <Field
                  component={RichTextEditorQuill}
                  placeholder="Enter a description"
                  label="Description2"
                  name="description2"
                />

                <Field
                  component={DateTimePicker}
                  label="Start At"
                  name="startAt"
                  placeholder="Select a Start date"
                />

                <Field
                  component={ImagePicker}
                  crop="free, 5:3"
                  label="Hero Image"
                  name="heroImage"
                />

                <Field name="timezone" component={TimezoneSelect} label="Timezone" />

                <Field
                  name="matcherinoUserPicker"
                  users={users}
                  label="Users"
                  component={MatcherinoUserPicker}
                  help="Test"
                />

                <Field name="recaptcha" component={ReCaptcha} />

                <div className="matcherinoUserPicker">
                  <UserLink user={user} />
                </div>
                <br />
              </FormSubmit>
            </Form>
          )}
        </Formik>
      </Container>
    );
  }
  validate = values => {
    let errors = {};

    if (values.firstName === '') {
      errors.firstName = 'Required';
    }

    if (values.lastName === '') {
      errors.lastName = 'Required';
    }

    if (values.age !== 25) {
      errors.age = `It's not your age`;
    }

    if (values.favoriteFood !== 'fried bangus' || values.favoriteFood === '') {
      errors.favoriteFood = `It's not your favorite food`;
    }

    if (values.description === '') {
      errors.description = 'Required';
    }

    if (values.description2 === '') {
      errors.description2 = 'Required';
    }

    if (values.startAt === null) {
      errors.startAt = 'Required';
    }

    if (values.heroImage === '') {
      errors.heroImage = 'Required';
    }

    if (values.timezone === '') {
      errors.timezone = 'Required';
    }

    // if (!values.matcherinoUserPicker || values.matcherinoUserPicker.userName !== 'chadmisa07') {
    //   errors.matcherinoUserPicker = 'Invalid User';
    // }

    if (!values.recaptcha) {
      errors.recaptcha = 'Required';
    }

    return errors;
  };
}

export default FormikForm;
