import React from 'react';
import Recaptcha from 'react-recaptcha';

// const secret = '6LeYnnUUAAAAALof_39zXYFBkobYYWijNU1mgBrD';
const sitekey = '6LfYpHUUAAAAAPKoj3KIZ_sTBh9BYfrq9mMWgPlF';

export default class x extends React.Component {
  callback = () => {
    console.log('Recaptcha successfully loaded');
  };

  // specifying verify callback function
  verifyCallback = response => {
    const {field, form} = this.props;

    if (response) {
      form.setFieldValue(field.name, true);
    }
  };

  render() {
    const {
      field,
      form: {touched, errors}
    } = this.props;
    return (
      <div>
        <Recaptcha
          sitekey={sitekey}
          render="explicit"
          verifyCallback={this.verifyCallback}
          onloadCallback={this.callback}
        />
        {touched[field.name] &&
          (errors[field.name] && (
            <span className="errors" style={{color: 'red'}}>
              {errors[field.name]}
            </span>
          ))}
        <br />
      </div>
    );
  }
}
