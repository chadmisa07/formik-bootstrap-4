import React from 'react';
import {Form} from 'react-bootstrap';
import _ from 'lodash';
import {propTypes, t} from 'tcomb-react';
import HelpText from './HelpText';

const Select = props => {
  const {
    field,
    help,
    form: {touched, errors},
    form,
    label,
    option,
    ...rest
  } = props;

  const options = _.map(option, (option, index) => {
    return (
      <option key={index} value={option.value}>
        {option.label}
      </option>
    );
  });

  const handleChange = e => {
    // this is going to call setFieldValue and manually update select value
    form.setFieldValue(field.name, e.target.value);
  };

  return (
    <Form.Group controlId={field.name}>
      <Form.Label>
        {label} {help && <HelpText help={help} />}
      </Form.Label>
      <Form.Control as="select" {...rest} onChange={handleChange} onBlur={field.onBlur}>
        <option value="" />
        {options}
      </Form.Control>
      {touched[field.name] &&
        (errors[field.name] && (
          <span className="errors" style={{color: 'red'}}>
            {errors[field.name]}
          </span>
        ))}
    </Form.Group>
  );
};

Select.defaultProps = {
  option: [{value: 0, text: ''}]
};

Select.displayName = 'Select';

Select.propTypes = propTypes({
  label: t.String,
  field: t.Object,
  form: t.maybe(t.Object),
  help: t.maybe(t.String),
  option: t.maybe(t.Array),
  children: t.ReactChild
});

export default Select;
