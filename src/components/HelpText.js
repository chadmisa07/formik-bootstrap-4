import * as React from 'react';
import PropTypes from 'prop-types';

//eslint-disable-next-line
const HelpText = ({help, className = 'icon-help-circled-alt'}) => {
  return (
    <span>
      <i className={className} title={help} />
    </span>
  );
};

HelpText.propTypes = {
  help: PropTypes.string.isRequired
};

export const HelpError = ({help}) => {
  return <HelpText help={help} className="icon-attention-circled text-danger" />;
};

HelpError.propTypes = {
  help: PropTypes.string.isRequired
};

export default HelpText;
