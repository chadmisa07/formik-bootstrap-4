import {propTypes, t} from 'tcomb-react';
import {default as DTP} from 'react-widgets/lib/DateTimePicker';
import Moment from 'moment';
import React from 'react';
import momentLocalizer from 'react-widgets-moment';
import {Form} from 'react-bootstrap';

momentLocalizer(Moment);

// function rounded(s) {
//   const d = new Date(s);
//   if (d.getMinutes() < 30) {
//     d.setMinutes(0);
//     d.setSeconds(0);
//     return d;
//   }

//   d.setMinutes(30);
//   d.setSeconds(0);
//   return d;
// }

const DateTimePicker = props => {
  const {
    field,
    placeholder,
    label,
    form: {touched, errors},
    form,
    name,
    ...rest
  } = props;

  const handleChange = e => {
    // this is going to call setFieldValue and manually update DateTimePicker value
    form.setFieldValue(field.name, e);
  };

  const handleBlur = () => {
    form.setFieldTouched(field.name, true);
  };
  return (
    <Form.Group>
      <Form.Label>{label}</Form.Label>
      <DTP
        {...field}
        name={name}
        // defaultValue={new Date()}
        placeholder={placeholder}
        onChange={handleChange}
        onBlur={handleBlur}
        time={true}
        {...rest}
      />
      {touched[field.name] &&
        (errors[field.name] && (
          <span className="errors" style={{color: 'red'}}>
            {errors[field.name]}
          </span>
        ))}
    </Form.Group>
  );
};

DateTimePicker.defaultProps = {
  text: null
};

DateTimePicker.displayName = 'DateTimePicker';

DateTimePicker.propTypes = propTypes({
  text: t.maybe(t.String),
  field: t.maybe(t.Object),
  help: t.maybe(t.String),
  label: t.maybe(t.String),
  placeholder: t.maybe(t.String),
  form: t.Object,
  children: t.ReactChild
});

export default DateTimePicker;
