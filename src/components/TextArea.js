import React from 'react';
import {Form} from 'react-bootstrap';
import {propTypes, t} from 'tcomb-react';
import HelpText from './HelpText';

const TextArea = props => {
  const {
    field,
    placeholder,
    label,
    form: {touched, errors},
    form,
    rows,
    help,
    ...rest
  } = props;

  const handleChange = e => {
    // this is going to call setFieldValue and manually update textarea value
    form.setFieldValue(field.name, e.target.value);
  };

  return (
    <Form.Group controlId={field.name}>
      <Form.Label>
        {label} {help && <HelpText help={help} />}
      </Form.Label>
      <Form.Control
        {...field}
        as="textarea"
        rows={rows}
        placeholder={placeholder}
        {...rest}
        onChange={handleChange}
      />
      {touched[field.name] &&
        (errors[field.name] && (
          <span className="errors" style={{color: 'red'}}>
            {errors[field.name]}
          </span>
        ))}
    </Form.Group>
  );
};

TextArea.defaultProps = {
  rows: 3
};

TextArea.propType = propTypes({
  field: t.Object,
  placeholder: t.maybe(t.String),
  label: t.maybe(t.String),
  form: t.Object,
  rows: t.maybe(t.String),
  helpt: t.maybe(t.String),
  children: t.ReactChild
});

export default TextArea;
