import React, {Component} from 'react';
import {propTypes, t} from 'tcomb-react';
import uploadcare from 'uploadcare-widget';
import {Form} from 'react-bootstrap';
import HelpText from './HelpText';

function initUploadCareOptions() {
  window.UPLOADCARE_PUBLIC_KEY = '7748e8ceaa0176aa1305';
  window.UPLOADCARE_MANUAL_START = true;
  window.UPLOADCARE_LOCALE_TRANSLATIONS = {
    buttons: {
      cancel: 'Clear'
    }
  };
}
initUploadCareOptions();

function isUploadCareImage(url) {
  return url && url.indexOf('//ucarecdn.com') > 0;
}

/**
 * Renders image preview with input box to choose or update
 * an image.
 */
export default class ImagePicker extends Component {
  static propTypes = propTypes({
    crop: t.maybe(t.String), // LxW pixels => "1600x450" or L:W ratio => "16:9"
    input: t.Object,
    label: t.maybe(t.String),
    placeholder: t.maybe(t.String),
    meta: t.Object
  });

  state = {
    cdnUrl: ''
  };

  componentDidMount() {
    const {doChange} = this;
    const {field} = this.props;

    const widget = uploadcare.Widget(this.uploader); // eslint-disable-line

    // for legacy images, we need to async upload the images to UploadCare
    const url = field.value;
    if (!isUploadCareImage(url)) {
      // some of our urls are borked and we need to prefix protocol before uploading
      const file = uploadcare.fileFrom('url', url);
      file
        .done(info => {
          this.doChange({cdnUrl: info.cdnUrl});
          widget.value(info.cdnUrl);
        })
        .fail(err => {
          console.log('error while uploading file ', err);
          this.doChange(null);
        });
    }

    widget.onChange(file => {
      if (file) {
        file
          .done(info => {
            doChange(info);
          })
          .fail(err => {
            console.log('uplaodcare ERROR ', err);
            this.doChange(null);
          });
        return;
      }

      doChange(null);
    });
  }

  handleChange = e => {
    const {field, form} = this.props;
    // this is going to call setFieldValue and manually update ImagePicker value
    form.setFieldValue(field.name, e.target.value);
  };

  render() {
    const {
      crop,
      field,
      form: {touched, errors},
      label,
      placeholder,
      help
    } = this.props;

    const url = field.value;
    const value = isUploadCareImage(url) ? url : null;

    const pholder = typeof placeholder === 'undefined' ? label || '' : placeholder;

    const imgEl = url ? <img src={url} alt="" /> : null;

    const inner = (
      <div className="image-picker">
        {imgEl}
        <input
          type="hidden"
          ref={c => (this.uploader = c)}
          placeholder={pholder}
          data-crop={crop}
          value={value}
          data-images-only="true"
        />
        {touched[field.name] &&
          (errors[field.name] && (
            <span className="errors" style={{color: 'red'}}>
              {errors[field.name]}
            </span>
          ))}
      </div>
    );

    if (label) {
      return (
        <Form.Group>
          <Form.Label>
            {label} {help && <HelpText help={help} />}
          </Form.Label>
          {inner}
        </Form.Group>
      );
    }

    return inner;
  }

  doChange = info => {
    if (!info || !info.cdnUrl) return;
    //console.log({info});
    const {field, form} = this.props;
    // this is going to call setFieldValue and manually update ImagePicker value
    form.setFieldValue(field.name, info.cdnUrl);
    form.setFieldTouched(field.name, true);
  };
}

ImagePicker.defaultProps = {
  text: ''
};

ImagePicker.displayName = 'ImagePicker';

ImagePicker.propTypes = propTypes({
  text: t.String,
  field: t.maybe(t.Object),
  help: t.maybe(t.String),
  label: t.maybe(t.String),
  placeholder: t.maybe(t.String),
  form: t.Object,
  crop: t.maybe(t.String),
  touched: t.maybe(t.Object),
  errors: t.maybe(t.Object),
  children: t.ReactChild
});
