import React, {Component} from 'react';
import ReactQuill from 'react-quill';
import {Form} from 'react-bootstrap';
import {propTypes, t} from 'tcomb-react';
import HelpText from './HelpText';

const modules = {
  toolbar: [
    [{header: [1, 2, 3, 4, 5, 6, false]}],
    ['bold', 'italic', 'underline', 'strike', 'blockquote', 'code-block'],
    [{script: 'sub'}, {script: 'super'}],
    [{color: []}, {background: []}],
    [{align: []}],
    [{list: 'ordered'}, {list: 'bullet'}, {indent: '-1'}, {indent: '+1'}],
    ['link', 'image', 'video'],
    ['clean']
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false
  }
};

const formats = [
  'header',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'code-block',
  'script',
  'color',
  'background',
  'align',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'video'
];

class RichtTextEditorQuill extends Component {
  handleChange = editorState => {
    const {field, form} = this.props;
    // this is going to call setFieldValue and manually update textarea value
    form.setFieldValue(field.name, editorState);
  };

  handleBlur = () => {
    const {field, form} = this.props;
    form.setFieldTouched(field.name, true);
  };

  render() {
    const {
      field,
      placeholder,
      label,
      form: {touched, errors},
      form,
      help,
      ...rest
    } = this.props;

    return (
      <Form.Group controlId={field.name}>
        <Form.Label>
          {label} {help && <HelpText help={help} />}
        </Form.Label>
        <ReactQuill
          {...field}
          placeholder={placeholder}
          modules={modules}
          formats={formats}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          {...rest}
        />
        {touched[field.name] &&
          (errors[field.name] && (
            <span className="errors" style={{color: 'red'}}>
              {errors[field.name]}
            </span>
          ))}
      </Form.Group>
    );
  }
}

RichtTextEditorQuill.defaultProps = {
  text: ''
};

RichtTextEditorQuill.displayName = 'RichtTextEditorQuill';

RichtTextEditorQuill.propTypes = propTypes({
  text: t.maybe(t.String),
  label: t.String,
  field: t.Object,
  placeholder: t.maybe(t.String),
  form: t.maybe(t.Object),
  help: t.maybe(t.String),
  touched: t.maybe(t.Object),
  errors: t.maybe(t.Object),
  children: t.ReactChild
});

export default RichtTextEditorQuill;
