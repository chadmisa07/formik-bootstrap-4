import React, {Component} from 'react';
import {propTypes, t} from 'tcomb-react';
import Select, {components} from 'react-select';
import UserLink from './UserLink';
import HelpText from './HelpText';

export default class MatcherinoUserPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedValue: props.users[2]
    };
  }
  optionChanged = value => {
    const {field, form} = this.props;
    this.setState({selectedValue: value});

    // this is going to call setFieldValue and manually update textarea value
    form.setFieldValue(field.name, value);
  };

  handleBlur = () => {
    const {field, form} = this.props;
    form.setFieldTouched(field.name, true);
  };

  render() {
    const {
      field,
      form: {errors, touched},
      label,
      users,
      help,
      ...rest
    } = this.props;

    const renderValue = props => {
      return props.data ? (
        <components.Option {...props}>
          <div>
            <UserLink
              user={props.data}
              disable={true}
              showPin={true}
              hideVerified={true}
              hideProvider={false}
            />
          </div>
        </components.Option>
      ) : (
        ''
      );
    };

    const ValueContainer = ({children, ...props}) => {
      return (
        <components.ValueContainer {...props}>
          {/* <span style={{marginLeft: '5px'}}>{children}</span> */}
          <UserLink
            user={this.state.selectedValue}
            disable={true}
            showPin={true}
            hideVerified={true}
            hideProvider={false}
          />
          {/* to allow select to be clickable*/}
          <span style={{visibility: 'hidden'}}>{children}</span>
        </components.ValueContainer>
      );
    };

    return (
      <div>
        {label && (
          <div>
            {label} {help && <HelpText help={help} />}
          </div>
        )}
        <Select
          {...field}
          components={{Option: renderValue, ValueContainer}}
          options={users}
          isFocused={false}
          isClearable
          openMenuOnClick={true}
          value={this.state.selectedValue}
          onChange={this.optionChanged}
          onBlur={this.handleBlur}
          {...rest}
        />
        {touched[field.name] &&
          (errors[field.name] && (
            <span className="errors" style={{color: 'red'}}>
              {errors[field.name]}
            </span>
          ))}
        <br />
      </div>
    );
  }
}

MatcherinoUserPicker.propTypes = propTypes({
  option: t.maybe(t.Array),
  field: t.Object,
  form: t.Object,
  users: t.Array,
  label: t.maybe(t.String),
  help: t.maybe(t.String),
  children: t.ReactChild
});
