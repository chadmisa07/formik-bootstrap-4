import React, {Component} from 'react';
import {EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';

class RichtTextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty()
    };
  }

  handleChange = editorState => {
    const {field, form} = this.props;
    // this is going to call setFieldValue and manually update textarea value
    form.setFieldValue(field.name, editorState);
  };

  render() {
    const {editorState} = this.state;
    const editorStyle = {
      border: '1px solid #F1F1F1',
      marginBottom: '10px',
      marginTop: '-6px',
      padding: '5px'
    };

    return (
      <Editor
        initialEditorState={editorState}
        wrapperClassName="rte-wrapper"
        editorClassName="rte-editor"
        onEditorStateChange={this.handleChange}
        editorStyle={editorStyle}
      />
    );
  }
}
export default RichtTextEditor;
