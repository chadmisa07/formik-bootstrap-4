import React from 'react';
import {Form} from 'react-bootstrap';
import HelpText from './HelpText';
import {propTypes, t} from 'tcomb-react';

const Input = props => {
  const {
    field,
    help,
    placeholder,
    form: {touched, errors},
    label,
    type,
    ...rest
  } = props;
  const classes = errors[field.name] ? 'has-error' : '';
  return (
    <Form.Group className={classes}>
      <Form.Label>
        {label} {help && <HelpText help={help} />}
      </Form.Label>
      <Form.Control {...field} type={type} placeholder={placeholder} {...rest} />

      {touched[field.name] &&
        (errors[field.name] && (
          <span className="errors" style={{color: 'red'}}>
            {errors[field.name]}
          </span>
        ))}
    </Form.Group>
  );
};

Input.defaultProps = {
  type: 'text'
};

Input.displayName = 'Input';

Input.propTypes = propTypes({
  field: t.maybe(t.Object),
  help: t.maybe(t.String),
  label: t.maybe(t.String),
  placeholder: t.maybe(t.String),
  type: t.maybe(t.String),
  form: t.Object,
  children: t.ReactChild
});

export default Input;
