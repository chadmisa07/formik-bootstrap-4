// import * as paths from '../common/paths';
import React, {Component} from 'react';
import AuthProviderLink from './AuthProviderLink';
import PropTypes from 'prop-types';
// import VerifiedSponsor from '#/client/components/Svgs/VerfiedSponsor';
import classNames from 'classnames';
// import {pure} from '../common/decorators';

/**
 * UserLink displays user avatar, links to both his matcherino profile and auth provider.
 */
// @pure
export default class UserLink extends Component {
  static propTypes = {
    disable: PropTypes.bool,
    displayNameClass: PropTypes.string,
    hideAvatar: PropTypes.bool,
    hideDisplayName: PropTypes.bool,
    hideProvider: PropTypes.bool,
    hideVerified: PropTypes.bool,
    showPin: PropTypes.bool,
    size: PropTypes.string, // sm, md, lg
    text: PropTypes.node,
    user: PropTypes.object.isRequired
  };

  static defaultProps = {
    disable: false,
    hideProvider: false,
    size: 'sm'
  };

  render() {
    const {
      displayNameClass,
      disable,
      hideDisplayName,
      showPin,
      hideProvider,
      text,
      user,
      hideAvatar,
      size,
      hideVerified
    } = this.props;

    let id,
      userName,
      displayName,
      displayNameOverride = '';

    if (user) {
      id = user.id;
      userName = user.userName;
      displayName = user.displayName;
      displayNameOverride = user.displayNameOverride;
    }
    // const {id, userName, displayName, displayNameOverride} = user;
    // const userProfile = paths.userVenues(id, displayName, authProvider);
    const userProfile = 'https://google.com';
    const classes = classNames('user-link', {DisableHover: disable});
    const displayNameClasses = classNames('user-link-name', displayNameClass);
    // const isOrganizer = user && user.canViewSponsoredRewards;
    const isOrganizer = true;
    // const isSponsor = user && user.canSponsorRewards;
    const isSponsor = true;

    const userLinkIconClass = id === 45 ? 'user-link-icon-square' : 'user-link-icon';

    return user ? (
      <div className={classes + ' ' + size}>
        {hideAvatar ? null : (
          <a href={userProfile} title={`${displayNameOverride || displayName || userName}#${id}`}>
            {/* <img className={userLinkIconClass} src={paths.userAvatar(user)} /> */}
            <img
              alt=""
              className={userLinkIconClass}
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/President_Rodrigo_Roa_Duterte_2017.jpg/225px-President_Rodrigo_Roa_Duterte_2017.jpg"
            />
          </a>
        )}
        {hideDisplayName ? null : (
          <a href={userProfile} className="flexboxerino">
            <span className={displayNameClasses} title={`#${id}`}>
              {displayNameOverride || displayName || userName}
            </span>
            {showPin && <span className="user-link-pin">#{id}</span>}
          </a>
        )}

        {isOrganizer && !hideVerified ? (
          <span className="user-link-wrap" title="Verified Organizer">
            <div className="user-verification">
              <i className=" icon-ok" />
              <i className=" icon-certificate" />
            </div>
          </span>
        ) : null}

        {isSponsor && !hideVerified ? (
          <span href="#" className="user-link-wrap sponsor" title="Verified Sponsor">
            {/* <VerifiedSponsor /> */}
            Verified Sponsor
          </span>
        ) : null}

        {hideProvider ? (
          <span>&nbsp;</span>
        ) : (
          <div className="user-link-provider-link">
            <AuthProviderLink user={user} disable={disable} />
          </div>
        )}

        {text && <span>{text}</span>}
      </div>
    ) : (
      <div />
    );
  }
}
