import React from 'react';
import {Button} from 'react-bootstrap';

const FormSubmit = props => {
  const {children, status} = props;
  return (
    <div>
      {children}
      <Button variant="primary" type="submit" disabled={status}>
        Submit
      </Button>
      <br />
      <br />
    </div>
  );
};

export default FormSubmit;
