// import * as paths from '../common/paths';
import {propTypes, t} from 'tcomb-react';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover from 'react-bootstrap/lib/Popover';
import React from 'react';
// import {pure} from '../common/decorators';

// @pure
export default class AuthProviderLink extends React.Component {
  static propTypes = propTypes({
    user: t.Object,
    disable: t.Boolean
  });

  renderTwitch() {
    const {userName, displayName} = this.props.user;
    return (
      <OverlayTrigger
        trigger={['hover', 'focus']}
        placement="top"
        overlay={<Popover id={'po-' + userName}>Visit {displayName} on Twitch</Popover>}
      >
        <a
          href={'https://google.com'}
          target="_blank"
          rel="noopener noreferrer"
          className="twitch-link"
        >
          <i className="icon-twitch icon-button" />
        </a>
      </OverlayTrigger>
    );
  }

  renderGoogle() {
    const {userName, displayName} = this.props.user;
    return (
      <OverlayTrigger
        trigger={['hover', 'focus']}
        placement="top"
        overlay={<Popover id={'po-' + userName}>Visit {displayName} on Google+</Popover>}
      >
        <a
          href={'https://google.com'}
          target="_blank"
          rel="noopener noreferrer"
          className="gplus-link"
        >
          <i className="login-icon gplus icon-button" />
        </a>
      </OverlayTrigger>
    );
  }

  renderTwitter() {
    const {userName, displayName} = this.props.user;
    return (
      <OverlayTrigger
        trigger={['hover', 'focus']}
        placement="top"
        overlay={<Popover id={'po-' + userName}>Visit {displayName} on Twitter</Popover>}
      >
        <a
          href={'https://google.com'}
          target="_blank"
          rel="noopener noreferrer"
          className="twitter-link"
        >
          <i className="icon-twitter icon-button" />
        </a>
      </OverlayTrigger>
    );
  }

  render() {
    const {user} = this.props;
    switch (user.authProvider) {
      case 'twitch':
        return this.renderTwitch();
      case 'gplus':
        return this.renderGoogle();
      case 'twitter':
        return this.renderTwitter();
      default:
        return null;
    }
  }
}
