import * as bountyModel from '../models/bountyModel';
import * as t from 'tcomb';
import _ from 'lodash';
import config from '../config.json';

export function front() {
  return '/';
}

export const userBounties = userPage('/tournaments');
export const userVenues = userPage('/events');
export const userProfile = userPage('/events');
export const userViewerGame = userPage('/viewergame');
export const userLPSettings = userPage('/lpsettings');
export const userLeaderboard = userPage('/leaderboard');
export const userWallet = userPage('/wallet');
export const userDLC = userPage('/dlc');
export const userLPPoints = userPage('/points');
export const userStore = userPage('/store');
export const userAnalytics = userPage('/analytics/dashboard');
export const userAnalyticsVenues = userPage('/analytics/venues');
export const userAnalyticsTournaments = userPage('/analytics/tournaments');
export const userAnalyticsRewards = userPage('/analytics/rewards');
export const userStoreAddProduct = userPage('/store/add');
export const userStoreManageProducts = userPage('/store/manage');
export const userStoreOverview = userPage('/store/overview');
export const userDonors = userPage('/donors');
export const userDonations = userPage('/donations');
export const userFollowers = userPage('/followers');
export const userFollowing = userPage('/following');
export const userContact = userPage('/contact');
export const userGames = userPage('/games');
export const userLandingPage = userPage('/landing');
export const userPasses = userPage('/passes');
export const userPurchases = userPage('/purchases');
export const userSettings = userPage('/account');
export const userCampaigns = userPage('/campaigns');
export const userDeleteMyAccount = userPage('/delete-my-account');
export const userProfileSettings = userPage('/settings');
export const userProfileRoles = userPage('/roles');
export const userDefault = userProfile;
export const storePurchase = rewardPage();
export const storeProfilePurchase = rewardPage();
export const storeBountyPurchase = rewardBountyPage();
export const userEditProduct = userPage('/store/manage');
export const rewardOverview = rewardPageOverView();
export const rewardProfileOverview = rewardProfilePageOverView();
export const campaignClickPage = campaignPage();
export const campaignWidget = campaignPageWidget();

export function avatar(url) {
  return url ? url.replace(/^https?:/, '') : appver('/img/avatar-default.png');
}

export function user(id, userName) {
  // eslint-disable-line no-unused-vars
  //return `/users/${authProvider}/${slug(userName)}/${id}`;
  return `${users()}/${encodeURIComponent(userName)}/${id}`;
}

function userPage(path) {
  return (id, userName, authProvider) => {
    return user(id, userName, authProvider) + path;
  };
}

export function userCampaignSubsection(usr, path) {
  const {id, userName, authProvider} = usr;
  return user(id, userName, authProvider) + '/campaigns' + path;
}

export function productPageTrans(id, userName, authProvider, pId) {
  return `${user(id, userName, authProvider)}/store/purchase/${pId}`;
}

export function venuePage(id) {
  return `/events/${id}`;
}

export function venueCheckin(id) {
  return `/events/${id}/checkin`;
}

function rewardPageOverView() {
  return pId => {
    return `/marketplace/reward/overview/${pId}`;
  };
}

function rewardProfilePageOverView() {
  return pId => {
    return `store/reward/overview/${pId}`;
  };
}

export function rewardOverviewPage(id, userName, pId) {
  return `${user(id, userName)}/store/reward/overview/${pId}`;
}

function rewardPage() {
  return pId => {
    return `/marketplace/reward/${pId}`;
  };
}

function rewardBountyPage() {
  return (pId, bId, type) => {
    return `/marketplace/reward/${pId}?sourceId=${bId}&sourceType=${type}`;
  };
}

function campaignPage() {
  return (rewardId, actionId, type) => {
    return `/marketplace/campaign/${rewardId}?sourceId=${actionId}&sourceType=${type}`;
  };
}

function campaignPageWidget() {
  return (rewardId, bountyId, type) => {
    return `/marketplace/campaign/${rewardId}/widget?sourceId=${bountyId}&sourceType=${type}`;
  };
}

export function cancelInvoice(id) {
  return `/invoices/${id}/cancel`;
}

/**
 *
 * @param {number} rewardId ID of the campaign reward.
 * @param {number} actionId  ID of the individual action.
 * @param {number} donationPoolId ID of the bounty's donation pool.
 */
export function actionClickPage(params) {
  const validate = t.struct({
    rewardId: t.Number,
    actionId: t.Number,
    donationPoolId: t.Number,
    authToken: t.maybe(t.String),
    authSecret: t.maybe(t.String)
  });
  validate(params);

  let url = `/marketplace/campaign/${params.rewardId}?actionId=${params.actionId}&poolId=${
    params.donationPoolId
  }`;
  if (params.authToken) {
    url += `&at=${encodeURIComponent(params.authToken)}`;
  }
  if (params.authSecret) {
    url += `&as=${encodeURIComponent(params.authSecret)}`;
  }

  return url;
}

//lost functionality with nav implimentation
// function editProductPage() {
//   return (id, userName, authProvider, pId) => {
//     return `${user(id, userName, authProvider)}/store/edit/${pId}`;
//   };
// }

export function matcherinoFullUrl(p) {
  return config.app.protocol + '://' + config.app.host + (p.startsWith('/') ? p : '/' + p);
}

export function oauth2Url(provider, sub) {
  return matcherinoFullUrl(`/__api/oauth2/${provider}/${sub}`);
}

export function loginPopup(provider) {
  return oauth2Url(provider, 'login');
}

export function logOut(provider) {
  return oauth2Url(provider, 'logout');
}

export function externalFrontPage() {
  return '//' + config.app.hostname;
}

export function bounties() {
  return '/b';
}

export function tournaments() {
  return '/tournaments';
}

export function venues() {
  return '/events/overview';
}

export function rewards() {
  return '/marketplace';
}

export function organizers() {
  return '/organizers';
}

//DEPRECATED
// export function events() {
//   return '/events';
// }

export function eventsTournaments() {
  return '/tournaments/overview';
}

export function eventsVenues() {
  return '/events/overview';
}

export function bountiesForKind(kind) {
  const url = checkVenueToEvents(`/${bountyModel.kindsMeta[kind].slug}`);
  return url;
}

export function bountyNew(kind, id, section = 'description') {
  const url = checkVenueToEvents(`/${bountyModel.kindsMeta[kind].slug}/${id}/${section}`);
  return url;
}

export {bountyNew as bountyKind};

export function userBountiesForKind(kind, userId, userName, authProvider) {
  const url = checkVenueToEvents(
    `${userBounties(userId, userName, authProvider)}/${bountyModel.kindsMeta[kind].slug}`
  );
  return url;
}

export function userDonationsForKind(kind, userId, userName, authProvider) {
  const url = checkVenueToEvents(
    `${userDonations(userId, userName, authProvider)}/${bountyModel.kindsMeta[kind].slug}`
  );
  return url;
}

export function game(id, title) {
  return `${slug(title)}/${id}`;
}

export function venue(id) {
  return `/events/${id}`;
}

export function bounty(id, title, gameId, gameTitle) {
  if (gameId) {
    return `/tournaments/${game(gameId, gameTitle)}/${slug(title)}/${id}`;
  }

  return `/tournaments/${id}`;
}

export function tournament(id) {
  return `/tournaments/${id}`;
}

export function bountyEdit(id) {
  return `${id}`;
}

export function bountyCreator() {
  return 'create';
}

export function createEventFor(id) {
  return `/create?profileId=${id}`;
}

export function users() {
  return '/u';
}

function slug(text) {
  return encodeURIComponent(_.kebabCase(text) || text);
}

export function appver(path) {
  return config.app.publicPath + path;
}

export const logo = appver('/img/favicon.png');

export const logoWhite = appver('/img/white-logo.png');

export const defaultAvatar = appver('/img/avatar-default.png');

export const marketplaceBanner = appver('/img/marketplaceBanner.jpg');

export const orgBanner = appver('/img/orgBanner.jpg');

export const eventsBanner = appver('/img/eventsBanner.jpg');

export const tournamentBanner = appver('/img/tournamentBanner.jpg');

export const defaultBG = appver('/img/default.jpg');

export const defaultEventIcon = appver('/img/default_venue_icon.png');

export const defaultEventHero = appver('/img/default_venue_cover.png');

export const defaultPlayerPassIcon = appver('/img/players.png');

export const defaultSpectatorPassIcon = appver('/img/spectator.png');

export const defaultUpsell = appver('/img/treasure.png');

export function twitchUser(userName) {
  return 'https://twitch.tv/' + userName;
}

export function gplusUser(authId) {
  return 'https://plus.google.com/' + authId;
}

export function twitterUser(userName) {
  return 'https://twitter.com/' + userName;
}

export function eventActions(kind, id) {
  const url = checkVenueToEvents(`/${bountyModel.kindsMeta[kind].slug}/${id}/admin/actions`);
  return url;
}

export function matcherino(p) {
  return config.app.protocol + '://' + config.app.host + (p[0] === '/' ? p : '/' + p);
}

export function defaultBountyThumbnail() {
  return appver('/img/64x64.png');
}

export function successCheck() {
  return appver('/img/Success-Checkmark.png');
}

export function defaultRewardImage() {
  return appver('/img/default_reward_image.jpg');
}

export function rewardImage(imageUrl) {
  return imageUrl ? urlFixer(imageUrl) : appver('/img/default_reward_image.jpg');
}

export function urlFixer(url) {
  return url.replace(/^(https?:)?(\/\/)?/, '//');
}

export function externalUrl(url) {
  if (!url) return null;

  if (url.startsWith('http')) {
    return url;
  }

  if (url.startsWith('//')) {
    return config.app.protocol + ':' + url;
  }

  return config.app.protocol + '://' + url;
}

function checkVenueToEvents(url) {
  return url.replace('/venues/', '/events/');
}

/**
 * Gets a low res image for the given URL image. The image is cached. POST
 * must be used to update the cached image.
 */
export function lowResImage(url) {
  let newUrl;

  // low res images are retrieved by server not by browser so it has to be
  // fully qualified
  if (url.startsWith('//')) {
    newUrl = 'https:' + url;
  } else if (url.startsWith('/')) {
    newUrl = matcherino(url);
  } else {
    newUrl = url;
  }

  return `/__img/proxy?url=${encodeURIComponent(newUrl)}`;
}

// calculates passed in user's avatar
export function userAvatar(usr) {
  if (!usr) return defaultAvatar;
  const url = (usr.profile && usr.profile.avatarUrl) || usr.avatar || defaultAvatar;
  return avatar(url);
}
